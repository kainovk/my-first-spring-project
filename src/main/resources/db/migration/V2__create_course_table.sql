CREATE TABLE courses
(
    id          LONG PRIMARY KEY NOT NULL,
    name        VARCHAR(64)      NOT NULL,
    description VARCHAR(64)
);

DROP TABLE IF EXISTS students;

CREATE TABLE students
(
    id        UUID PRIMARY KEY,
    name      VARCHAR(64) NOT NULL,
    age       int         NOT NULL,
    course_id LONG,
    CONSTRAINT FK_STUDENTS_COURSES FOREIGN KEY (course_id) REFERENCES courses (id)
);
