package ru.tinkoff.fintech.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.tinkoff.fintech.cache.CourseCache;
import ru.tinkoff.fintech.dao.CourseRepository;
import ru.tinkoff.fintech.model.Course;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@AllArgsConstructor
@Service
public class CourseService {

    private final CourseRepository repository;

    private final CourseCache cache;

    public void save(Course course) {
        repository.save(course);
        cache.save((repository.findBySpecification(course.getName(), course.getDescription(), course.getRequired_grade())).get());
    }

    public Course findById(UUID id) {
        Optional<Course> courseToFind = cache.get(id);
        if (courseToFind.isPresent()) {
            return courseToFind.get();
        } else {
            Course courseFound = repository.findById(id).orElseThrow();
            cache.save(courseFound);
            return courseFound;
        }
    }

    public List<Course> findAll(List<UUID> ids) {
        return repository.findAll(ids);
    }

    public void update(Course course) {
        repository.update(course);
        cache.update(course);
    }

    public void delete(UUID id) {
        repository.delete(id);
        cache.delete(id);
    }
}
