package ru.tinkoff.fintech.dto;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CourseRequest {

    @NotEmpty
    String name;
    @NotEmpty
    String description;
    @NotNull
    @Range(min = 0, max = 5)
    Integer required_grade;
}
