package ru.tinkoff.fintech.controller;

import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.fintech.dto.CourseRequest;
import ru.tinkoff.fintech.model.Course;
import ru.tinkoff.fintech.service.CourseService;

import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@RestController
@RequestMapping("/course")
public class CourseController {

    public static final Logger log = LoggerFactory.getLogger(CourseController.class);

    private final CourseService courseService;

    @PostMapping("/create")
    public void addCourse(@RequestBody @Validated CourseRequest request) {
        Course course = Course.builder()
                .name(request.getName())
                .description(request.getDescription())
                .required_grade(request.getRequired_grade())
                .build();
        courseService.save(course);
        log.info("Course {} has been added!", course.getName());
    }

    @GetMapping("/get/{id}")
    public Course getCourse(@PathVariable UUID id) {
        return courseService.findById(id);
    }

    @GetMapping("/get")
    public List<Course> getCourses(@RequestParam(required = false) List<UUID> id) {
        return courseService.findAll(id);
    }

    @PutMapping("/update/{id}")
    public void updateCourse(@PathVariable UUID id, @RequestBody @Validated CourseRequest request) {
        Course course = Course.builder()
                .name(request.getName())
                .description(request.getDescription())
                .required_grade(request.getRequired_grade())
                .build();
        courseService.update(course);
        log.info("Course {} with id={} has been updated!", course.getName(), id);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteCourse(@PathVariable UUID id) {
        courseService.delete(id);
        log.info("Course with id={} has been deleted!", id);
    }
}
