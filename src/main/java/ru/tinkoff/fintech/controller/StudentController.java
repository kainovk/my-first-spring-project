package ru.tinkoff.fintech.controller;

import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.fintech.dto.AddToCourseRequest;
import ru.tinkoff.fintech.dto.StudentRequest;
import ru.tinkoff.fintech.model.Student;
import ru.tinkoff.fintech.service.StudentService;

import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@RestController
@RequestMapping("/student")
public class StudentController {

    public static final Logger log = LoggerFactory.getLogger(StudentController.class);

    private final StudentService studentService;

    private final KafkaTemplate<String, String> kafkaTemplate;

    public void sendMessage(String msg) {
        kafkaTemplate.send("000m6ke2-fintech-out", msg);
    }

    @PostMapping("/create")
    public void addStudent(@RequestBody @Validated StudentRequest request) {
        Student student = Student.builder()
                .name(request.getName())
                .age(request.getAge())
                .grade(request.getGrade())
                .build();
        studentService.save(student);
        log.info("Student with name={} has been added!", student.getName());

        sendMessage(student.toString());
    }

    @GetMapping("/get")
    public List<Student> getStudents(@RequestParam(required = false) List<UUID> id) {
        return studentService.findAll(id);
    }

    @PutMapping("/update/{id}")
    public void updateStudent(@PathVariable UUID id, @RequestBody @Validated StudentRequest request) {
        Student student = Student.builder()
                .name(request.getName())
                .age(request.getAge())
                .grade(request.getGrade())
                .build();
        studentService.update(student);
        log.info("Student with id={} has been updated!", id);

        sendMessage(student.toString());
    }

    @DeleteMapping("/delete/{id}")
    public void deleteStudent(@PathVariable UUID id) {
        studentService.delete(id);
        log.info("Student with id={} has been deleted!", id);

        sendMessage("Student with id=" + id + " deleted");
    }

    @PutMapping("/join")
    public void addToCourse(
            @RequestBody @Validated(AddToCourseRequest.AddToCourseRequestValidationSequence.class) AddToCourseRequest request
    ) {
        studentService.addToCourse(request.getCourseId(), request.getStudentIds());
    }
}
