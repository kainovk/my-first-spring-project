package ru.tinkoff.fintech.validation;

import lombok.RequiredArgsConstructor;
import ru.tinkoff.fintech.dao.CourseRepository;
import ru.tinkoff.fintech.dao.StudentRepository;
import ru.tinkoff.fintech.dto.AddToCourseRequest;
import ru.tinkoff.fintech.exception.EntityNotFoundException;
import ru.tinkoff.fintech.model.Course;
import ru.tinkoff.fintech.model.Student;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

@RequiredArgsConstructor
public class GradeValidator implements ConstraintValidator<GradeConstraint, AddToCourseRequest> {

    private final CourseRepository courseRepository;
    private final StudentRepository studentRepository;

    @Override
    public boolean isValid(AddToCourseRequest request, ConstraintValidatorContext ctx) {
        Course course = Optional.ofNullable(courseRepository.findAll(List.of(request.getCourseId())))
                .filter(Predicate.not(List::isEmpty))
                .map(it -> it.get(0))
                .orElseThrow(() -> new EntityNotFoundException(Course.class));

        List<Student> students = Optional.ofNullable(studentRepository.findAll(request.getStudentIds()))
                .filter(Predicate.not(List::isEmpty))
                .filter(it -> request.getStudentIds().size() == it.size())
                .orElseThrow(() -> new EntityNotFoundException(Student.class));

        return students.stream().map(Student::getGrade).allMatch(it -> it >= course.getRequired_grade());
    }
}
