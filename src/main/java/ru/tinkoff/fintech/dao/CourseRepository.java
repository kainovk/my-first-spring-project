package ru.tinkoff.fintech.dao;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import ru.tinkoff.fintech.model.Course;

@Mapper
public interface CourseRepository {

    void save(Course course);

    Optional<Course> findById(UUID id);

    Optional<Course> findBySpecification(String name, String description, int requiredGrade);

    List<Course> findAll(@Param("ids") List<UUID> ids);

    void update(Course course);

    void delete(UUID id);
}
