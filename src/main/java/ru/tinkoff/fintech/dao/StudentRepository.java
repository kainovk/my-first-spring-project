package ru.tinkoff.fintech.dao;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import ru.tinkoff.fintech.model.Student;

@Mapper
public interface StudentRepository {

    void save(Student student);

    Optional<Student> findById(UUID id);

    List<Student> findAll(@Param("ids") List<UUID> ids);

    void update(Student student);

    void delete(UUID id);

    void addToCourse(UUID courseId, @Param("studentIds") List<UUID> studentIds);
}
