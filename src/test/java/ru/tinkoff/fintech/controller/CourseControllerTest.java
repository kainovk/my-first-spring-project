package ru.tinkoff.fintech.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.web.servlet.MockMvc;
import ru.tinkoff.fintech.AbstractTest;
import ru.tinkoff.fintech.model.Course;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
class CourseControllerTest extends AbstractTest {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private MockMvc mockMvc;

    private final ObjectMapper jackson = new ObjectMapper();

    @Test
    void testAddNewCourseSuccess() throws Exception {
        var course = prepareCourse(UUID.randomUUID());
        var courseJson = jackson.writeValueAsString(course);
        mockMvc.perform(post("/course/create")
                        .contentType("application/json")
                        .content(courseJson))
                .andExpect(status().isOk());

    }

    @Test
    void testAddNewCourseBlankName() throws Exception {
        var course = prepareCourseBlankName(UUID.randomUUID());
        var courseJson = jackson.writeValueAsString(course);

        mockMvc.perform(post("/course/create")
                        .contentType("application/json")
                        .content(courseJson))
                .andDo(print())
                .andExpect(status().is(400));
    }

    @Test
    void testAddNewCourseBlankDescription() throws Exception {
        var course = prepareCourseBlankDescription(UUID.randomUUID());
        var courseJson = jackson.writeValueAsString(course);

        mockMvc.perform(post("/course/create")
                        .contentType("application/json")
                        .content(courseJson))
                .andDo(print())
                .andExpect(status().is(400));
    }

    @Test
    void testAddNewCourseInvalidReqGrade() throws Exception {
        var course = prepareCourseInvalidReqGrade(UUID.randomUUID());
        var courseJson = jackson.writeValueAsString(course);

        mockMvc.perform(post("/course/create")
                        .contentType("application/json")
                        .content(courseJson))
                .andDo(print())
                .andExpect(status().is(400));
    }

    @Test
    void testGetSuccess() throws Exception {
        var course = prepareCourse(UUID.randomUUID());
        populateDb(course);
        var courseJson = jackson.writeValueAsString(course);

        mockMvc.perform(get("/course/get")
                        .param("id", course.getId().toString()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string("[" + courseJson + "]"));
    }

    @Test
    void testUpdateCourseSuccess() throws Exception {
        var course = prepareCourse(UUID.randomUUID());
        populateDb(course);

        course.setRequired_grade(4);
        course.setDescription("Fintech Java Course");
        var changedCourseJson = jackson.writeValueAsString(course);

        mockMvc.perform(put("/course/update/" + course.getId().toString())
                        .contentType("application/json")
                        .content(changedCourseJson))
                .andExpect(status().isOk());
    }

    @Test
    void testDeleteCourseSuccess() throws Exception {
        var course = prepareCourse(UUID.randomUUID());
        populateDb(course);

        mockMvc.perform(delete("/course/delete/" + course.getId().toString()))
                .andExpect(status().isOk());
    }

    @Test
    void testDeleteCourseNotExists() throws Exception {
        mockMvc.perform(delete("/course/delete")
                        .param("id", UUID.randomUUID().toString()))
                .andExpect(status().is(404));
    }

    private void populateDb(Course course) {
        jdbcTemplate.update("""
                            INSERT INTO courses (id, name, description, required_grade)
                            VALUES ( ?,?,?,? )
                """, ps -> {
            ps.setString(1, course.getId().toString());
            ps.setString(2, course.getName());
            ps.setString(3, course.getDescription());
            ps.setInt(4, course.getRequired_grade());
        });
    }


    private Course prepareCourse(UUID id) {
        return Course.builder()
                .id(id)
                .name("Java")
                .description("Fintech")
                .required_grade(4)
                .build();
    }

    private Course prepareCourseBlankName(UUID id) {
        return new Course(
                id,
                "",
                "Fintech",
                4
        );
    }

    private Course prepareCourseBlankDescription(UUID id) {
        return new Course(
                id,
                "Java",
                "",
                4
        );
    }

    private Course prepareCourseInvalidReqGrade(UUID id) {
        return new Course(
                id,
                "Java",
                "Fintech",
                -1
        );
    }
}