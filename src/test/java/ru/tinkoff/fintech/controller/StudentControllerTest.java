package ru.tinkoff.fintech.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.web.servlet.MockMvc;
import ru.tinkoff.fintech.AbstractTest;
import ru.tinkoff.fintech.model.Student;

import java.util.ArrayList;
import java.util.UUID;

import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
class StudentControllerTest extends AbstractTest {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private MockMvc mockMvc;

    private final ObjectMapper jackson = new ObjectMapper();

    @Test
    void testAddNewStudentSuccess() throws Exception {
        var student = prepareStudent(UUID.randomUUID());
        var studentJson = jackson.writeValueAsString(student);
        mockMvc.perform(post("/student/create")
                        .contentType("application/json")
                        .content(studentJson))
                .andExpect(status().isOk());

    }

    @Test
    void testAddNewStudentInvalidGrade() throws Exception {
        var student = prepareStudentInvalidGrade(UUID.randomUUID());
        var studentJson = jackson.writeValueAsString(student);

        mockMvc.perform(post("/student/create")
                        .contentType("application/json")
                        .content(studentJson))
                .andDo(print())
                .andExpect(status().is(400));
    }

    @Test
    void testAddNewStudentInvalidAge() throws Exception {
        var student = prepareStudentInvalidAge(UUID.randomUUID());
        var studentJson = jackson.writeValueAsString(student);

        mockMvc.perform(post("/student/create")
                        .contentType("application/json")
                        .content(studentJson))
                .andDo(print())
                .andExpect(status().is(400));
    }

    @Test
    void testAddNewStudentBlankName() throws Exception {
        var student = prepareStudentBlankName(UUID.randomUUID());
        var studentJson = jackson.writeValueAsString(student);

        mockMvc.perform(post("/student/create")
                        .contentType("application/json")
                        .content(studentJson))
                .andDo(print())
                .andExpect(status().is(400));
    }

    @Test
    void testGetSuccess() throws Exception {
        var student = prepareStudent(UUID.randomUUID());
        populateDb(student);
        var studentJson = jackson.writeValueAsString(student);

        mockMvc.perform(get("/student/get")
                        .param("id", student.getId().toString()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string("[" + studentJson + "]"));
    }

    @Test
    void testUpdateStudentSuccess() throws Exception {
        var student = prepareStudent(UUID.randomUUID());
        populateDb(student);

        student.setGrade(1);
        student.setAge(55);
        student.setName("Kirill2");
        var changedStudentJson = jackson.writeValueAsString(student);

        mockMvc.perform(put("/student/update/" + student.getId().toString())
                        .contentType("application/json")
                        .content(changedStudentJson))
                .andExpect(status().isOk());
    }

    @Test
    void testDeleteStudentSuccess() throws Exception {
        var student = prepareStudent(UUID.randomUUID());
        populateDb(student);

        mockMvc.perform(delete("/student/delete/" + student.getId().toString()))
                .andExpect(status().isOk());
    }

    @Test
    void testDeleteStudentNotExists() throws Exception {
        mockMvc.perform(delete("/student/delete")
                        .param("id", UUID.randomUUID().toString()))
                .andExpect(status().is(404));
    }

    private void populateDb(Student student) {
        jdbcTemplate.update("""
                            INSERT INTO students (id, name, age, grade)
                            VALUES ( ?,?,?,? )
                """, ps -> {
            ps.setString(1, student.getId().toString());
            ps.setString(2, student.getName());
            ps.setInt(3, student.getAge());
            ps.setInt(4, student.getGrade());
        });
    }

    private Student prepareStudent(UUID id) {
        return Student.builder()
                .id(id)
                .name("Kirill")
                .age(20)
                .grade(4)
                .courseIds(new ArrayList<>())
                .build();
    }

    private Student prepareStudentInvalidGrade(UUID id) {
        return Student.builder()
                .id(id)
                .name("Kirill")
                .age(20)
                .grade(-5)
                .build();
    }

    private Student prepareStudentInvalidAge(UUID id) {
        return new Student(
                id,
                "Kirill",
                0,
                4
        );
    }

    private Student prepareStudentBlankName(UUID id) {
        return new Student(
                id,
                "",
                0,
                4
        );
    }
}